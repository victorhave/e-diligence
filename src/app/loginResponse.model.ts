export class LoginResponseModel {
	access_token:string;
	token_type:string;
	expires_in:string;
	cod_usu:string;
	cod_cliente:string;
	usuario:any;
}