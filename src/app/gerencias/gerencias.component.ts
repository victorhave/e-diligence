import { GerenciasService } from './../gerencias.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-gerencias',
  templateUrl: './gerencias.component.html',
  styleUrls: ['./gerencias.component.css']
})
export class GerenciasComponent implements OnInit {
  title:any="Gerências";
  editando = 0; // -> 0=Cadastrando ----- 1=Editando
  gerenciaForm: FormGroup;
  gerencia:any={Cod_Gerencia:0,Gerencia:''};

  constructor(private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private gerenciaService: GerenciasService) {

      this.gerenciaForm = this.formBuilder.group({
        Cod_Gerencia: 0,
        Cod_Cliente:0,//vai sair
        Gerencia: ['', Validators.required]
      });

    }

  ngOnInit() {

    let id = +this.activatedRoute.snapshot.paramMap.get('idGerencia');

    if(id != 0) {
      this.obter(id);
    }

  }
  voltar()
  {
    this.router.navigate(['/gerencias']);
  }
  setForm(gerencia)
  {
    this.gerenciaForm.patchValue({
      Cod_Gerencia: gerencia.Cod_Gerencia,
      Cod_Cliente: gerencia.Cod_Cliente,//vai sair
      Gerencia: gerencia.Gerencia
    })
  }

  setGerencia(gerencia)
  {
    this.gerencia = gerencia;
  }

  setTitle(title)
  {
    this.title = title;
  }

  obter(Cod_Gerencia) //método utilizado para alteração de uma Gerencia
  {

      this.gerenciaService.buscaCodigo(Cod_Gerencia).subscribe(gerencia => {
                                                          this.setGerencia(gerencia[0]);
                                                          this.setTitle('Editando Gerência: ' + gerencia[0].Gerencia);
                                                          this.setForm(gerencia[0]);  
                                                          this.editando = 1;
                                                        }, 
      err => console.log(err));
                                                      
  }
  incluir(gerencia)
  {
    if(this.editando == 0){ //quando a variavel "editando" for 0, o formulário é para cadastro
    
      this.gerenciaService.incluir(gerencia).subscribe(
          data => { 
              alert("Gerencia cadastrada com sucesso!");
              this.router.navigate(['/gerencias']);
          },
          err => {
            alert("Erro ao cadastrar gerencia! Entre em contato com a equipe UNICAD e informe o seguinte erro: "+ err);
          }
      );
    }else if(this.editando == 1){//quando a variável não é 0, é para realizar a alteração de algum dado
      
      this.alterar(gerencia);
    }
  }

  alterar(gerencia)
  {
    this.gerenciaService.alterar(gerencia).subscribe(
        data => {
          if(data.status==="ok"){
            alert("Dados alterados com sucesso!");
            this.router.navigate(['/gerencias']);
          }else{
            alert("Erro na alteração dos dados!");
          }
      },
        err => console.log(err)
      );
  }

  salvar(gerencia)
  {
    if(gerencia.Cod_Equipe == 0)
    {
      this.incluir(gerencia);
      this.router.navigate(['/gerencias']);
    }
    else
    {
      this.alterar(gerencia);
    }
  }
}
