import { Component, OnInit, Input } from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {UsuarioModel} from '../usuario.model';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() title:string='e-Diligence';
	@Input() is_logado:boolean=false;
	@Input() usuario_id:string;
	Usuario:UsuarioModel;
  AuthenticationService:AuthenticationService;
  
  cur_url:string=window.location.href;

  constructor(AuthenticationService : AuthenticationService, private router: Router) {
    this.AuthenticationService = AuthenticationService;
    this.Usuario = AuthenticationService.getUsuario();    
   }

  ngOnInit() {
  }

  logout() {
		
		//console.log(this.AuthenticationService);
		let teste = this.AuthenticationService.logoutSession();
		//console.log(this.AuthService.getSession());
		this.AuthenticationService.logout();
		this.router.navigate(['./login']);
  }
  
  setLang(strLang) {
		localStorage.removeItem('lang');
		localStorage.setItem('lang', strLang);
		document.location.reload();
	}

}
