import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { API } from '../../app.config';
import { environment } from '../environments/environment';
import { Http, Headers, Response, RequestMethod, RequestOptions, ResponseOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EquipeService {
  url = environment.api_domain_url + '/equipes';
  constructor(public http: Http) { }

  listar(url)
  {
    if(url != ''){
      return this.http.get(url).map(res => res.json());//caso o acesso venha de fora da tela de equipes
    }else{
      return this.http.get(this.url).map(res => res.json());//caso o acesso venha da tela de equipes
    }
      
  }

  obter(equipe)
  {
    let params: HttpParams = undefined;
    params = new HttpParams().append('Nome_Equipe', equipe);
    
    return this.http.get(this.url + '/' + equipe, {params:params})
      .map(res => res.json());
  }

  buscaCodigo(idEquipe){
    let params: HttpParams = undefined;
    params = new HttpParams().append('Cod_Equipe', idEquipe);
    console.log(this.url.slice(0, -1) + '/' + idEquipe);
    //aqui eu removo o último caractere da string de URL para retirar o "s" de EquipeS e mandar pra rota correta (sem o s)
    return this.http.get(this.url.slice(0, -1) + '/' + idEquipe, {params:params})
      .map(res => res.json());
  }

  incluir(equipe)
  {
      // let headers = new Headers();
      // headers.append('Content-Type', 'application/json');

      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      
      return this.http.post(this.url+'-incluir', equipe, options).map(
        (res : Response) =>  {return res.json()}
      );
  }

  alterar(equipe)
  {

      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers});
     
      return this.http.put(this.url+'/' + equipe.Cod_Equipe, equipe, options).map(
        (res : Response) =>  {return res.json()}
      );
  }

  excluir(equipe)
  {
      let headers = new Headers({'Content-Type' : 'application/json'});
      let options = new RequestOptions({ headers: headers, method: RequestMethod.Delete });
      return this.http.delete(this.url + '/' + equipe.Cod_Equipe, options).map(
        (res : Response) =>  {return res.json();}
      );
  }
}
