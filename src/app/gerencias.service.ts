import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Http, Headers, Response, RequestMethod, RequestOptions, ResponseOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GerenciasService {
  url = environment.api_domain_url + '/gerencias';
  constructor(public http: Http) { }

  listar()
  {
      return this.http.get(this.url).map(res => res.json());
  }
  incluir(gerencia)
  {
      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      
      return this.http.post(this.url+'-incluir', gerencia, options).map(//nessa linha estou enviando via POST o objeto para o Laravel via URL pré definida na classe de rotas API
        (res : Response) =>  {return res.json()}
      );
  }
  excluir(gerencia)
  {
      let headers = new Headers({'Content-Type' : 'application/json'});
      let options = new RequestOptions({ headers: headers, method: RequestMethod.Delete });
      return this.http.delete(this.url + '/' + gerencia.Cod_Gerencia, options).map(
        (res : Response) =>  {return res.json();}
      );
  }

  obter(equipe)
  {
    let params: HttpParams = undefined;
    params = new HttpParams().append('Gerencia', equipe);
    
    return this.http.get(this.url + '/' + equipe, {params:params})
      .map(res => res.json());
  }

  buscaCodigo(idGerencia){
    let params: HttpParams = undefined;
    params = new HttpParams().append('Cod_Gerencia', idGerencia);
//    console.log(this.url.slice(0, -1) + '/' + idGerencia);
    //aqui eu removo o último caractere da string de URL para retirar o "s" de EquipeS e mandar pra rota correta (sem o s)
    return this.http.get(this.url.slice(0, -1) + '/' + idGerencia, {params:params})
      .map(res => res.json());
  }
  alterar(gerencia)
  {
      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers});

      return this.http.put(this.url+'/' + gerencia.Cod_Gerencia, gerencia, options).map(
        (res : Response) =>  {return res.json()}
      );
  }

}
