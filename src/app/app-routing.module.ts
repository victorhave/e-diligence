import { GerenciaListComponent } from './gerencia-list/gerencia-list.component';
import { EquipeMembrosComponent } from './equipe-membros/equipe-membros.component';
import { EquipeListComponent } from './equipe-list/equipe-list.component';
import { DiligenciasComponent } from './diligencias/diligencias.component';
import { EquipeComponent } from './equipe/equipe.component';
import { Error404Component } from './error404/error404.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GerenciasComponent } from './gerencias/gerencias.component';
import { MembroComponent } from './membro/membro.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},

  {path: 'equipes', component: EquipeListComponent},
  {path: 'equipes/:nome', component: EquipeListComponent},//Passa o objeto com o nome da equipe para filtrar
  {path: 'equipe/:idEquipe', component: EquipeListComponent},
  {path: 'equipes/editar/:idEquipe', component: EquipeComponent},//Editar Equipe
  {path: 'equipes-incluir', component: EquipeComponent},

  {path: 'equipes-membros/:idEquipe', component: EquipeMembrosComponent},
  {path: 'equipes-membros/:idEquipe/incluir', component: MembroComponent},//Formulário de cadastro do membro na equipe
  {path: 'equipes-membros/:idEquipe/editar/:idMembro', component: MembroComponent},//Formulário de alteração do membro da equipe

  {path: 'gerencias', component: GerenciaListComponent},//lista de gerências
  {path: 'gerencias/editar/:idGerencia', component: GerenciasComponent},//Editar Equipe
  {path: 'gerencias-incluir', component: GerenciasComponent},//formulário para cadastro de gerência

  {path: 'diligencias', component: DiligenciasComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: '**', component: Error404Component}
];
@NgModule({
  exports:[ RouterModule],
  imports: [
    CommonModule,RouterModule.forRoot(routes)
  ],
  declarations: []
})

export class AppRoutingModule { }
