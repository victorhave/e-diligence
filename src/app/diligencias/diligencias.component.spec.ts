import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiligenciasComponent } from './diligencias.component';

describe('DiligenciasComponent', () => {
  let component: DiligenciasComponent;
  let fixture: ComponentFixture<DiligenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiligenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiligenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
