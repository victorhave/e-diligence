import { GerenciaModalComponent } from './../gerencia-modal/gerencia-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EquipeService } from './../equipe.service';
import { Component, OnInit, Inject,ChangeDetectorRef  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePickerComponent } from 'ngx-bootstrap/datepicker/datepicker.component';
import { environment } from '../../environments/environment';
import { GerenciasService } from '../gerencias.service';

@Component({
  selector: 'app-diligencias',
  templateUrl: './diligencias.component.html',
  styleUrls: ['./diligencias.component.css']
})
export class DiligenciasComponent implements OnInit {
  url = environment.api_domain_url;
  title = 'Cadastro de Diligências';
  diligenciasForm: FormGroup;
  editando = 0;// -> 0=Cadastrando ----- 1=Editando
  diligencia:any={Cod_Diligencia:0,codCliente:0,Cod_Usu:0,Descricao:'',Objetivo:'',Dt_Inicio:'',DtFim:''};


  
  clusters:any=[]; //variavel cluster foi criada para preencher a tela enquanto não chega o retorno da API do Jesus...
  equipe : any = []; // Recebe as equipes cadastradas para preencher o select do formulário
  gerencia : any = []; // Recebe as gerências cadastradas para preencher o select do formulário


  gerenciasSelecionadas:any=[];
  dtInicio: Date = new Date();

  bsModalRef: BsModalRef;//variavel para mostrar um MODAL

  alertsGerencia: any = []; // variavel utilizada para mostrar uma nova gerência adicionada...
  maxGerencias = 0;
  alertsEquipe: any = []; // variavel utilizada para mostrar uma nova Equipe adicionada...
  maxEquipe = 0;
  alertsCluster: any = []; // variavel utilizada para mostrar um novo Cluster adicionado...
  maxCluster = 0;
  alertsAnexo:any=[]; //variável para adicionar todos os anexos selecionados pelo usuário....



  atualizaGerencias(cod){
    let select:String = cod.target.name;
    let value:String = cod.target.value;
    select = select.substring(select.indexOf("-")+1);

console.log(select);
    if(this.gerenciasSelecionadas.indexOf(cod) > -1 || cod == 0){
      //console.log("Já marcado!");
    }else{
      this.gerenciasSelecionadas.push([select,value]);
      //console.log("Não marcado ainda! -> " + cod + "\n->" + this.gerenciasSelecionadas);      
    }
    let auxJaExiste = 0;
    this.gerenciasSelecionadas.forEach(element => {
      if(element[0] == select){
        auxJaExiste = 1; //informo ao sistema que o index selecionado já existe no sitema
      }
      //console.log(element[0] + " - " + element[1]);
    });
    console.log(auxJaExiste);
  }






  constructor(private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private equipeService: EquipeService,
    private cdr: ChangeDetectorRef,
    private gerenciaService: GerenciasService,
    private modalService: BsModalService) {

      this.diligenciasForm = this.formBuilder.group({
        Cod_Diligencia: 0,
        Cod_Cliente: [0, Validators.required],
        Cod_Gerencia: [this.gerenciasSelecionadas, Validators.required],
        Cod_Equipe: [0, Validators.required],
        Cod_Usu:0,
        Cod_Cluster:0,
        Compendio:[''],
        Descricao: ['', Validators.required],
        Objetivo: ['', Validators.required],
        Dt_Inicio: ['', Validators.required],
        Dt_Fim: [''],
        Anexo:[''],
      });
  }

  ngOnInit() {
    this.listarEquipes();
    this.listarGerencias();//busca todas as gerências
    this.listarClusters();
    /*
    let id = +this.activatedRoute.snapshot.paramMap.get('idDiligencia');

    if(id != 0) {
      this.obter(id);
    }*/
    
  }
  /*
  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(GerenciaModalComponent, {initialState});//adiciona o componente ao modal
    this.bsModalRef.content.closeBtnName = 'Close';
  }
  */

  //--------------------------------------------------ADD/REMOVE GERENCIA-----------------------------------------------
addGerencia(): void {
    if((Object.keys(this.alertsGerencia).length+1) <= Object.keys(this.gerencia).length){
      this.alertsGerencia.push({
        msg: `Qtd de gerências bate com o que está no BD #pas`
      });
    }else{
      this.maxGerencias = 1;//exibe o tooltip de número máximo de gerências atingido
    }
  }
  removeGerencia(position):void{//remove a posição selecionada de gerências
    this.alertsGerencia.splice(position,1);
    this.maxGerencias = 0;//remove a exibição do tooltip de número máximo de gerências
  }
//-----------------------------------------------END ADD/REMOVE EQUIPE----------------------------------------------



//--------------------------------------------------ADD/REMOVE EQUIPE-----------------------------------------------
addEquipe(): void {
  if((Object.keys(this.alertsEquipe).length+1) <= Object.keys(this.equipe).length){
    this.alertsEquipe.push({
      msg: `Qtd de equipes bate com o que está no BD #pas`
    });
  }else{
    this.maxEquipe = 1;//exibe o tooltip de número máximo de equipes atingido
  }
}
removeEquipe(position):void{//remove a posição selecionada de equipes
  this.alertsEquipe.splice(position,1);
  this.maxEquipe = 0;//remove a exibição do tooltip de número máximo de equipes
}
//-----------------------------------------------END ADD/REMOVE EQUIPE-----------------------------------------------

//--------------------------------------------------ADD/REMOVE CLUSTER-----------------------------------------------
addCluster(): void {
  if((Object.keys(this.alertsCluster).length+1) <= Object.keys(this.clusters).length){
    this.alertsCluster.push({
      msg: `Qtd de clusters bate com o que está no BD #pas`
    });
  }else{
    /* ---->Após realizar a busca do cluster, remover este comentário para que o sistema funcione perfeitamente
    this.maxCluster = 1;//exibe o tooltip de número máximo de clusters atingido
    */
  }
}
removeCluster(position):void{//remove a posição selecionada de cluster
  this.alertsCluster.splice(position,1);
  this.maxCluster = 0;//remove a exibição do tooltip de número máximo de cluster
}
//-----------------------------------------------END ADD/REMOVE CLUSTER-----------------------------------------------

//--------------------------------------------------ADD/REMOVE ANEXO-----------------------------------------------
addAnexo(): void {
    this.alertsAnexo.push({
      msg: `Mais um Anexo para esta diligência #pas`
    });
}
removeAnexo(position):void{//remove a posição selecionada de anexo
  this.alertsAnexo.splice(position,1);
  //this.maxCluster = 0;// ->>>> O usuário pode adicionar a quantidade que quiser de itens anexos...
}
//-----------------------------------------------END ADD/REMOVE ANEXO-----------------------------------------------


  listarEquipes()
  {
    this.equipeService.listar(this.url+'/equipes').subscribe(
      equipe  => {this.equipe = equipe;},
      error => console.log(error)
    );
  }
  listarGerencias()
  {
    this.gerenciaService.listar().subscribe(
      gerencia  => {this.gerencia = gerencia;},
      error => console.log(error)
    );
  }
  listarClusters(){
    //Aqui vai entrar a função que preenche o cluster no select desta pagina... por enquanto está estático

  }

  obter(Cod_Diligencia) //método utilizado para alteração de uma diligência
  {
/* //--->Remover o comentário quando chegar na etapa da alteração
      this.equipeService.buscaCodigo(Cod_Diligencia).subscribe(equipe => {
                                                          this.setEquipe(equipe[0]);
                                                          this.setTitle('Editando Equipe: ' + equipe[0].Nome_Equipe);
                                                          this.setForm(equipe[0]);  
                                                          this.editando = 1;
                                                        }, 
      err => console.log(err));
      */
  }
  voltar()
  {
    this.router.navigate(['/diligencias']);
  }
  setForm(diligencia)
  {
    this.diligenciasForm.patchValue({
      Cod_Diligencia: diligencia.Cod_Equipe,
      Cod_Cliente: diligencia.CodCliente,
      Cod_Usu:diligencia.Cod_Usu,
      Nome_Equipe: diligencia.Nome_Equipe
    })
  }

  setDiligencia(diligencia)
  {
    this.diligenciasForm = diligencia;
  }

  setTitle(title)
  {
    this.title = title;
  }


  incluir(diligencia){
    
    console.log(this.gerenciasSelecionadas);
    console.log("\n"+diligencia.Cod_Gerencia);
  }

}
