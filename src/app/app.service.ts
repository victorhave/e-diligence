import { Injectable, Input } from '@angular/core';

@Injectable()
export class AppService {

	title:string='e-Diligence'
	prevForm:any;

	constructor() { }

	setTitle(title:string) {
		this.title = title;
	}

	@Input() getTitle() {
		return 'dsadsadsa'+this.title;
	}

	setPrev(prevForm) {
		console.log(prevForm);
		this.prevForm = prevForm;
	}

	getPrev() {
		return this.prevForm;
	}

}
