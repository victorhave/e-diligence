import { TestBed, inject } from '@angular/core/testing';

import { GerenciasService } from './gerencias.service';

describe('GerenciasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GerenciasService]
    });
  });

  it('should be created', inject([GerenciasService], (service: GerenciasService) => {
    expect(service).toBeTruthy();
  }));
});
