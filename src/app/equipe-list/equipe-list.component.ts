import { EquipeService } from './../equipe.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalContainerComponent } from 'ngx-bootstrap/modal/modal-container.component';

@Component({
  selector: 'app-equipe-list',
  templateUrl: './equipe-list.component.html',
  styleUrls: ['./equipe-list.component.css']
})
export class EquipeListComponent implements OnInit {
  title = 'Lista de Equipes';
  
  //Criei uma matriz de clientes apenas para testar, enquanto não tenho a consulta dos clientes implementada.
  clientes:any=[{id : '1',nome : 'Petrorio'},{id : '2',nome : 'Embratel'},{id : '3',nome : 'Eletrobras'}];

  equipeForm: FormGroup;
  equipe : any = [];
  //equipe:any={Cod_Equipe:0,codCliente:0,Nome_Equipe:'',created_at:'',updated_at:''};
  constructor(
    private BsModalRef:BsModalRef,
    private modalService:BsModalService,
    private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private equipeService: EquipeService) {

      //this.activatedRoute.params.subscribe(res => console.log(res.id));

      this.equipeForm = this.formBuilder.group({
        Nome_Equipe: ['']
      });
   }

  ngOnInit() {
		this.listar();
  }

  incluir()
  {
    this.router.navigate(['equipes-incluir']);
  }

  alterar(equipe)
  {
    //console.log('/equipes/editar/'+equipe.Cod_Equipe);
    this.router.navigate(['/equipes/editar/'+equipe.Cod_Equipe]);
  }

  excluir(equipe)
  {
    let c = confirm('Tem certeza que deseja excluir esta equipe?');
    if(c)
    {      
      this.equipeService.excluir(equipe).subscribe(
        data => {console.log(data.message);if(data.status=="ok"){alert("Equipe excluída com sucesso!");this.listar();}else{alert("Erro");this.listar();} },
        err => console.log(err)
      );
    }
  }
  filtrar(equipe){
    
    this.equipeService.obter(equipe.Nome_Equipe).subscribe(
      equipe  => {this.equipe = equipe;},
      error => console.log(error)
    );
  }
  membros(equipe){
    this.router.navigate(['/equipes-membros/' + equipe.Cod_Equipe]);
  }
  listar()
  {
    this.equipeService.listar('').subscribe(
      equipe  => {this.equipe = equipe;},
      error => console.log(error)
    );
  }

}
