export class UsuarioModel {
	cod_usu:number;
	login:string;
	nome:string;
	cliente_nome:string;
	aplicacao_nome:string;
	access_token:string='';
	expires_in:string;
	senha:string;
	cod_cliente:number;

	constructor(cod_usu, login, senha, token, exp) {
		this.cod_usu = cod_usu;
		this.login = login;
		this.senha = senha;
		this.access_token = token;
		this.expires_in = exp;
	}

	getLogin() {
		return this.login;
	}

	setCodCliente(codCliente) {
		this.cod_cliente = codCliente;
	}
	setUsuario(cod_usu){
		this.cod_usu = cod_usu;
	}
	setNome(nome) {
		this.nome = nome;
	}

	getCodCliente() {
		return this.cod_cliente;
	}
}
