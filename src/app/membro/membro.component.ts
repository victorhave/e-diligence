import { MembrosService } from './../membros.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxBrModule } from 'ngx-br';

@Component({
  selector: 'app-membro',
  templateUrl: './membro.component.html',
  styleUrls: ['./membro.component.css']
})
export class MembroComponent implements OnInit {
  
  title = 'Equipes';
  membroForm: FormGroup;
  editando = 0;
  idEquipe=0;
  idMembro=0;
  constructor(private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private membroService: MembrosService) {

    this.membroForm = this.formBuilder.group({
      Cod_Membro: 0,
      Cod_Equipe:0,
      Nome: ['', Validators.required],
      Mail: '',
      Telefone:'',
      Celular:'',
      Cargo:''
    });
    
   }

   ngOnInit() {

    this.idEquipe = +this.activatedRoute.snapshot.paramMap.get('idEquipe');
    this.setEquipe();
    this.idMembro = +this.activatedRoute.snapshot.paramMap.get('idMembro');

    if(this.idMembro != 0) {
      this.obter(this.idMembro);
    }

  }
  setEquipe(){
    this.membroForm.patchValue({
      Cod_Equipe: this.idEquipe
    })
  }

  setMembro(membro){}

  voltar()
  {
    this.router.navigate(['/equipes-membros/'+this.idEquipe]);
  }

  obter(Cod_Membro) //método utilizado para busca p/ alteração de um membro da equipe
  {
    this.membroService.buscaCodigo(Cod_Membro).subscribe(membro => {
                                                          this.setMembro(membro[0]);
                                                          this.setTitle('Editando Equipe: ' + membro[0].Nome);
                                                          this.setForm(membro[0]);  
                                                          this.editando = 1;
                                                        }, 
      err => console.log(err));
  }
  setTitle(titulo){
    this.title = titulo;
  }
  setForm(membro){
    this.membroForm.patchValue({
      Cod_Membro: membro.Cod_Membro,
      Cod_Equipe: membro.Cod_Equipe,
      Nome: membro.Nome,
      Mail: membro.Mail,
      Telefone: membro.Telefone,
      Celular: membro.Celular,
      Cargo: membro.Cargo
    })
  }
  incluir(membro)
  {
    if(this.editando == 0){ //quando a variavel "editando" for 0, o formulário é para cadastro
    
      this.membroService.incluir(membro).subscribe(
          data => { 
              alert("Membro cadastrado com sucesso!");
              this.router.navigate(['/equipes-membros/'+this.idEquipe]);
          },
          err => {
            alert("Erro ao cadastrar membro! Entre em contato com a equipe UNICAD e informe o seguinte erro: "+ err);
          }
      );
    }else if(this.editando == 1){//quando a variável não é 0, é para realizar a alteração de algum dado
      //->quando estiver montando a alteração, liberar essa linha
      this.alterar(membro);
    }
  }

  alterar(membro)
  {
    this.membroService.alterar(membro).subscribe(
        data => {console.log(data);
          if(data.status==="ok"){
            alert("Dados alterados com sucesso!");
            this.router.navigate(['/equipes-membros/'+membro.Cod_Equipe]);
          }else{
            alert("Erro na alteração dos dados!");
          }
      },
        err => console.log(err)
      );
  }

}
