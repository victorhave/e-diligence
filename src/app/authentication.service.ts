import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UsuarioModel} from './usuario.model';
import { Observable } from 'rxjs';
import {LoginResponseModel} from './loginResponse.model';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthenticationService {

    //protected urlLogin:string='http://192.168.1.102/laravel-api/api/auth/login';
	protected urlLogin:string=environment.api_domain_url+'/login';
	Usuario:UsuarioModel;
	is_logado:boolean=false;
	usuario_id:string;

    public token: string;
    //private url = 'http://localhost:4040/login';
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers });

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		let url: string = state.url;
		return this.checkLogin(url);
	}
    constructor(private router: Router, private http: HttpClient) {
        // Atribui o token se ele estiver salvo no local storage
        //var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //this.token = currentUser && currentUser.token;
        console.log('1');
	//	this.getSession();  -> comentei para funcionar o resto da aplicação sem travar no login

		/*

		if (['/login', '/logout'].indexOf(router.url) == -1) {
			this.is_logado = this.verificaLogin();
			if (!this.is_logado) {
				console.log('3');
				this.router.navigate(['/login']);
			}
		}*/
    }
    verificaLogin():boolean {

		if (localStorage.getItem('User')) {
			// let User = JSON.parse(localStorage.getItem('User'));
			this.Usuario = JSON.parse(localStorage.getItem('User'));
			this.is_logado=true;
			this.usuario_id = this.Usuario.nome;
			//console.log(this.Usuario);
			//console.log(localStorage.getItem('lang'));
			return true;
		}
		return false;
    }
    makeLogin(params): Promise<void> {		
		return this.http.post<any>(this.urlLogin, params).toPromise()
		.then(this.extractData)
		.catch(error => console.log(error));
	}
    async login (params) {
		// console.log(params);

		let testeRetorno= await this.makeLogin(params);

		if (this.verificaLogin()) {
			// console.log(this.Usuario);
			// console.log('----------------------');
			return true;
		}

		return false;
	}
    private extractData(res) {
		let UserLogin:LoginResponseModel = res;		
		let access_token:string = UserLogin.access_token;

		let expires_in:string = UserLogin.expires_in;
		let usuario:UsuarioModel = new UsuarioModel(UserLogin.cod_usu, UserLogin.usuario.login, UserLogin.usuario.senha, UserLogin.access_token, UserLogin.expires_in);
		usuario.setCodCliente(UserLogin.cod_cliente);
		usuario.setNome(UserLogin.usuario.nome);
		localStorage.setItem('User', JSON.stringify(usuario));
		// console.log('1');
		//this.Usuario = usuario;

		// localStorage.setItem('User', JSON.stringify(usuario));
				// console.log(usuario);
				// this.Usuario = usuario;
				// this.is_logado=true;
		//return body.data || {};
	}
	logoutSession():Promise<void> {
		let retorno = this.http.get<any>(environment.api_domain_url+'/deleteSession')
		.toPromise()
		.then(data => {console.log(data)})
		.catch(error => console.log(error));

		console.log(retorno);
		return retorno;
	}
    getSession():Promise<void> {		
		let request_url:string = window.location.href;
		return this.http.get<any>(environment.api_domain_url+'/getSession')
		.toPromise()
		.then(
			async data => {
				let res = JSON.parse(localStorage.getItem('User'));
				if (res) {
					// console.log(res);
					// console.log(res.cod_usu+'||'+data.COD_USU);
					if (res.cod_usu != data.COD_USU) {
						// console.log('desloga ');
						localStorage.removeItem('User');
					} else {
						console.log('mesmo usuario ou sem res');
						// console.log(res);
					}
				}
				let params = {login:data.LOGIN, senha:data.SENHA};
				// console.log(params)
				let testeRetorno = await this.makeLogin(params);
				// console.log('2');

				let verify = this.verificaLogin();

				if (this.verificaLogin()) {
					// console.log(res);
					if (res == null || res.cod_usu != data.COD_USU) {
						// console.log('redirect');
						// console.log(request_url);
						// console.log(window.location.origin);
						if (request_url != window.location.origin+'/login') {
							document.location.href = request_url;
							// this.router.navigate(['/panorama-semanal']);
						}
					}
				}
				console.log('2');
			})
		.catch(error => console.log(error));
		
	}


	getUsuario() {
		return this.Usuario;
	}

	getToken() {
		return this.Usuario.access_token;
	}

	getCodUsu() {
		return this.Usuario.cod_usu;
	}

    checkLogin(url: string): boolean { return true }
    logout(): void {
        // Limpa o token removendo o usuário do local store para efetuar o logout
        this.token = null;
        localStorage.removeItem('currentUser');
    }
    refreshToken() {		
		let params = {login:this.Usuario.login, senha:this.Usuario.senha};				
		let access_token:string;
		this.http.post<any>(this.urlLogin, params)
		.subscribe(res => {			
			if (typeof res === 'object') {
				localStorage.removeItem('User');
				access_token = res.access_token;
				let expires_in:string = res.expires_in;
				let usuario:UsuarioModel = new UsuarioModel(res.cod_usu, params.login, params.senha, res.access_token, res.expires_in);
				usuario.setCodCliente(res.cod_cliente);
				localStorage.setItem('User', JSON.stringify(usuario));
				// console.log(usuario);
				this.verificaLogin();

				return usuario.access_token;
			}			
		});
		
	}
}