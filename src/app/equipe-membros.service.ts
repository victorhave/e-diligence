import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Http, Headers, Response, RequestMethod, RequestOptions, ResponseOptions } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class EquipeMembrosService {
  url = environment.api_domain_url + '/equipes-membros';
  constructor(public http: Http) { }
  idEquipe:any=0;

  listar(equipe)
  {
    let params: HttpParams = undefined;
    params = new HttpParams().append('Cod_Equipe', equipe);
    return this.http.get(this.url + '/' + equipe, {params:params}).map(res => res.json());
  }

}
