import { AuthenticationService } from '../authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  rForm: FormGroup;
  
  constructor(private fb: FormBuilder, public AuthenticationService:AuthenticationService, private router: Router) {
		this.rForm = fb.group({
			'email' : ['', Validators.required],
			'senha' : ['', Validators.required],
		});
  }
  async login(values) {
    /* ---->linhas comentadas para o desenvolvimento de outros módulos
		let params = {login:values.email, senha:values.senha};
		let loginReturn = await this.AuthenticationService.login(params);
		
		if (loginReturn) {
			// console.log('aki');
			// console.log(localStorage.getItem('User'));
			this.router.navigate(['/panorama-semanal']);
		} else {
			alert('Erro ao logar, por favor, verifique seus dados');
    }*/
    this.router.navigate(['/home']);
	}

  ngOnInit() {
    this.AuthenticationService.logout();
  }

}