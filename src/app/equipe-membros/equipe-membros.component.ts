import { MembrosService } from './../membros.service';
import { EquipeMembrosService } from './../equipe-membros.service';
//import { EquipeService } from './../equipe.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EquipeService } from '../equipe.service';

@Component({
  selector: 'app-equipe-membros',
  templateUrl: './equipe-membros.component.html',
  styleUrls: ['./equipe-membros.component.css']
})
export class EquipeMembrosComponent implements OnInit {
  
  title = 'Membros da Equipe';
  membros:any={codMembro:0, codEquipe:0,nomeEquipe:'', Nome:'', Email:'', Telefone:'', Celular:'', created_at:'', updated_at:''};
  equipe:any={Cod_Equipe:0,Nome_Equipe:''};//busco os dados da equipe no service de equipes
  cont : number = 0;
 codEquipe=0;
 NomeEquipe='';
  constructor(private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private membrosService: MembrosService,
    private equipeService: EquipeService) {

  }

  ngOnInit() {
    let id = +this.activatedRoute.snapshot.paramMap.get('idEquipe');
    this.codEquipe = id;
    if(id != 0) {
      this.listar(id);
    }
    
  }
  setContatos(telefone, celular){
    let contatos:any='';
    if(telefone != ''){
      contatos = telefone;
    }
    if(contatos!=''){
      if(celular !=''){
        contatos += ' / ' + celular;
      }
    }else{
      if(celular != ''){
        contatos = celular;
      }
    }
    if(contatos!=''){
      return contatos;
    }else{
      return '---';
    }
  }
  listar(idEquipe)//busca todos os membros daquela equipe
  {
    this.equipeService.buscaCodigo(idEquipe).subscribe( //busca o ID e o NOME da equipe
      equipe  => {this.equipe = equipe;},
      error => console.log(error)
    );
    this.membrosService.listar(idEquipe).subscribe(//busca os membros da equipe
      membros  => {this.membros = membros;},
      error => console.log(error)
    );
  }
  voltar()
  {
    this.router.navigate(['/equipes']);
  }
  incluir(){
    this.router.navigate(['equipes-membros/'+ this.codEquipe +'/incluir']);
  }
  alterar(membro)
  {
    this.router.navigate(['equipes-membros/'+this.codEquipe+'/editar/'+membro.Cod_Membro]);
  }

  excluir(membro)
  {
    let c = confirm('Tem certeza que deseja excluir este membro da equipe?');
    if(c)
    {      
      this.membrosService.excluir(membro).subscribe(
        data => {
          if(data.status=="ok"){
            alert("Membro excluído com sucesso!");
            this.listar(this.codEquipe);
          }else{alert("Erro");
            this.listar(this.codEquipe);
        } 
      },
        err => console.log(err)
      );
    }
  }
}
