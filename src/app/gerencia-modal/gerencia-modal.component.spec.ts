import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciaModalComponent } from './gerencia-modal.component';

describe('GerenciaModalComponent', () => {
  let component: GerenciaModalComponent;
  let fixture: ComponentFixture<GerenciaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
