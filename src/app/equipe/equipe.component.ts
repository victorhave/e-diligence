import { EquipeService } from './../equipe.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.component.html',
  styleUrls: ['./equipe.component.css']
})
export class EquipeComponent implements OnInit {
  title = 'Equipes';
  equipeForm: FormGroup;
  editando = 0;// -> 0=Cadastrando ----- 1=Editando
  equipe:any={Cod_Equipe:0,codCliente:0,Nome_Equipe:''};
  cont : number = 0;

  constructor(private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private equipeService: EquipeService) {

      this.equipeForm = this.formBuilder.group({
        Cod_Equipe: 0,
        codCliente:0,
        Nome_Equipe: ['', Validators.required]
      });

  }

  ngOnInit() {

    let id = +this.activatedRoute.snapshot.paramMap.get('idEquipe');

    if(id != 0) {
      this.obter(id);
    }

  }

  voltar()
  {
    this.router.navigate(['/equipes']);
  }

  addMembros(){
    //abrir lightbox
    //Aqui vai entrar o comando para abrir o formulário de membros da equipe em uma lightbox
    alert("Lightbox para adicionar membros na equipe");
  }
  setForm(equipe)
  {
    this.equipeForm.patchValue({
      Cod_Equipe: equipe.Cod_Equipe,
      codCliente: equipe.CodCliente,
      Nome_Equipe: equipe.Nome_Equipe
    })
  }

  setEquipe(equipe)
  {
    this.equipe = equipe;
  }

  setTitle(title)
  {
    this.title = title;
  }

  obter(Cod_Equipe) //método utilizado para alteração de uma equipe
  {
      this.equipeService.buscaCodigo(Cod_Equipe).subscribe(equipe => {
                                                          this.setEquipe(equipe[0]);
                                                          this.setTitle('Editando Equipe: ' + equipe[0].Nome_Equipe);
                                                          this.setForm(equipe[0]);  
                                                          this.editando = 1;
                                                        }, 
      err => console.log(err));
  }

  incluir(equipe)
  {
    if(this.editando == 0){ //quando a variavel "editando" for 0, o formulário é para cadastro
    
      this.equipeService.incluir(equipe).subscribe(
          data => { 
              alert("Equipe cadastrada com sucesso!");
              this.router.navigate(['/equipes']);
          },
          err => {
            alert("Erro ao cadastrar equipe! Entre em contato com a equipe UNICAD e informe o seguinte erro: "+ err);
          }
      );
    }else if(this.editando == 1){//quando a variável não é 0, é para realizar a alteração de algum dado
      
      this.alterar(equipe);
    }
  }

  alterar(equipe)
  {
    this.equipeService.alterar(equipe).subscribe(
        data => {
          if(data.status==="ok"){
            alert("Dados alterados com sucesso!");
            this.router.navigate(['/equipes']);
          }else{
            alert("Erro na alteração dos dados!");
          }
      },
        err => console.log(err)
      );
  }

  salvar(equipe)
  {
    if(equipe.Cod_Equipe == 0)
    {
      this.incluir(equipe);
      this.router.navigate(['/equipes']);
    }
    else
    {
      this.alterar(equipe);
    }
  }
}
