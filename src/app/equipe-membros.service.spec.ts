import { TestBed, inject } from '@angular/core/testing';

import { EquipeMembrosService } from './equipe-membros.service';

describe('EquipeMembrosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EquipeMembrosService]
    });
  });

  it('should be created', inject([EquipeMembrosService], (service: EquipeMembrosService) => {
    expect(service).toBeTruthy();
  }));
});
