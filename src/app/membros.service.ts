import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { API } from '../../app.config';
import { environment } from '../environments/environment';
import { Http, Headers, Response, RequestMethod, RequestOptions, ResponseOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class MembrosService {
  url = environment.api_domain_url + this.router.url;
  idEquipe=0;
  id = this.activatedRoute.snapshot.paramMap.get('idEquipe');
  
  constructor(public http: Http,
    private router : Router,
    private activatedRoute: ActivatedRoute) { 
    
  }
  
  listar(equipe)
  {
    let params: HttpParams = undefined;
    params = new HttpParams().append('Cod_Equipe', equipe);
    return this.http.get(environment.api_domain_url + '/equipes-membros' + '/' + equipe, {params:params}).map(res => res.json());
  }
  incluir(equipe)
  {
      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      console.log(this.url);
      return this.http.post(this.url, equipe, options).map(
        (res : Response) =>  {return res.json()}
      );
  }

  alterar(membro)
  {

      let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers});
     //console.log(membro);
      return this.http.put(environment.api_domain_url + '/alterar-membro-equipe/' + membro.Cod_Membro, membro, options).map(
        (res : Response) =>  {return res.json()}
      );
  }

  excluir(membro)
  {
      let headers = new Headers({'Content-Type' : 'application/json'});
      let options = new RequestOptions({ headers: headers, method: RequestMethod.Delete });
      
      return this.http.delete(environment.api_domain_url + '/apagar-membro-equipe/' + membro.Cod_Membro, options).map(
        (res : Response) =>  {return res.json();}
      );
  }

  buscaCodigo(idMembro){
    let params: HttpParams = undefined;
    params = new HttpParams().append('Cod_Membro', idMembro);
   //Padrão, a busca de 1 unico elemento não fica no plural na URL \/
    return this.http.get(environment.api_domain_url+'/equipes-membro/' + idMembro, {params:params})
      .map(res => res.json());
  }
}
