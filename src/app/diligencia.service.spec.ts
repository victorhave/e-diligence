import { TestBed, inject } from '@angular/core/testing';

import { DiligenciaService } from './diligencia.service';

describe('DiligenciaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiligenciaService]
    });
  });

  it('should be created', inject([DiligenciaService], (service: DiligenciaService) => {
    expect(service).toBeTruthy();
  }));
});
