import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from './authentication.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { Error404Component } from './error404/error404.component';
import { AlertModule } from 'ngx-bootstrap';
import { HeaderComponent } from './header/header.component';

import { AppService } from './app.service';
import { EquipeMembrosComponent } from './equipe-membros/equipe-membros.component';
import { EquipeComponent } from './equipe/equipe.component';
import { GerenciasComponent } from './gerencias/gerencias.component';
import { DiligenciasComponent } from './diligencias/diligencias.component';
import { EquipeService } from './equipe.service';
import { EquipeListComponent } from './equipe-list/equipe-list.component';
import { ComponentLoaderFactory } from 'ngx-bootstrap/component-loader/component-loader.factory';
import { PositioningService } from 'ngx-bootstrap/positioning/positioning.service';
import { EquipeMembrosService } from './equipe-membros.service';
//import { ModalBackdropComponent } from 'ngx-bootstrap/modal/modal-backdrop.component';
import { MembroComponent } from './membro/membro.component';
import { MembrosService } from './membros.service';
import { NgxBrModule } from 'ngx-br';
import { GerenciasService } from './gerencias.service';
import { GerenciaListComponent } from './gerencia-list/gerencia-list.component';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GerenciaModalComponent } from './gerencia-modal/gerencia-modal.component';
import { ModalContainerComponent } from 'ngx-bootstrap/modal/modal-container.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    Error404Component,
    HeaderComponent,
    EquipeMembrosComponent,
    EquipeComponent,
    GerenciasComponent,
    DiligenciasComponent,
    EquipeListComponent,
    MembroComponent,
    GerenciaListComponent,
    GerenciaModalComponent
  ],
  imports: [
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxBrModule
  ],
  providers: [EquipeMembrosService,
    PositioningService, 
    AppService, 
    DatePipe,
    AuthenticationService,
    EquipeService,
    MembrosService,
    BsModalRef,
    BsModalService,
    ComponentLoaderFactory,BsModalService,GerenciasService
  ],
  bootstrap: [AppComponent],
  entryComponents:[GerenciaModalComponent]
})
export class AppModule { }
