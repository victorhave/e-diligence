import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciaListComponent } from './gerencia-list.component';

describe('GerenciaListComponent', () => {
  let component: GerenciaListComponent;
  let fixture: ComponentFixture<GerenciaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
