import { GerenciasService } from '../gerencias.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalContainerComponent } from 'ngx-bootstrap/modal/modal-container.component';

@Component({
  selector: 'app-gerencia-list',
  templateUrl: './gerencia-list.component.html',
  styleUrls: ['./gerencia-list.component.css']
})
export class GerenciaListComponent implements OnInit {
  title = 'Lista de Gerências';
  
  gerenciaForm: FormGroup;
  gerencia : any = [];

  constructor(
    private BsModalRef:BsModalRef,
    private modalService:BsModalService,
    private formBuilder: FormBuilder, 
    private router : Router,
    private activatedRoute: ActivatedRoute, 
    private gerenciaService: GerenciasService) {

      //this.activatedRoute.params.subscribe(res => console.log(res.id));

    this.gerenciaForm = this.formBuilder.group({
      Gerencia: ['']
    });
      
   }

  ngOnInit() {
		this.listar();
  }

  incluir()
  {
    this.router.navigate(['gerencias-incluir']);
  }

  alterar(gerencia)
  {
    //console.log('/equipes/editar/'+equipe.Cod_Equipe);
    this.router.navigate(['/gerencias/editar/'+gerencia.Cod_Gerencia]);
  }

  excluir(gerencia)
  {
    let c = confirm('Tem certeza que deseja excluir esta gerência?');
    if(c)
    {      
      this.gerenciaService.excluir(gerencia).subscribe(
        data => {if(data.status=="ok"){alert("Gerência excluída com sucesso!");this.listar();}else{alert("Erro");this.listar();} },
        err => console.log(err)
      );
    }
  }
  filtrar(gerencia){
    this.gerenciaService.obter(gerencia.Gerencia).subscribe(
      gerencia  => {this.gerencia = gerencia;},
      error => console.log(error)
    );
  }

  listar()
  {
    this.gerenciaService.listar().subscribe(
      gerencia  => {this.gerencia = gerencia;},
      error => console.log(error)
    );
  }

}
